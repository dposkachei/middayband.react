import React, {Component, Suspense} from 'react';
import {Redirect, Route, Switch} from 'react-router-dom';
import { YMInitializer } from 'react-yandex-metrika';

import routes from '../../routes';
import {Dimmer, Loader} from "semantic-ui-react";

class DefaultLayout extends Component {
    _isMounted = false;
    state = {};

    constructor(props) {
        super(props);
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
    }

    componentWillMount() {
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    loading = () => (
        <div>
            <Dimmer active inverted>
                <Loader size='large'/>
            </Dimmer>
        </div>
    );

    render() {
        if (!this.shouldComponentRender()) return ('');
        return (
            <div className={`app`}>
                <Suspense fallback={this.loading()}>
                    <Switch>
                        {routes.map((route, idx) => {
                            return route.component ? (
                                <Route
                                    key={idx}
                                    path={route.path}
                                    exact={route.exact}
                                    name={route.name}
                                    render={props => (
                                        <route.component
                                            {...props}
                                        />
                                    )}
                                />
                            ) : null;
                        })}
                        <Redirect from="/" to="/"/>
                    </Switch>
                </Suspense>
                {process.env.NODE_ENV === 'production' &&
                <YMInitializer accounts={[58876432]} />
                }
            </div>
        );
    }
}

export default DefaultLayout;
