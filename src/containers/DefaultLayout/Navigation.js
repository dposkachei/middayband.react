import React, {Component} from 'react';
import {Menu, Responsive} from 'semantic-ui-react';

import ScrollspyNav from "react-scrollspy-nav";

class Navigation extends Component {
    constructor(props) {
        super(props);
        this.state = {
            mobileNav: {
                active: false,
            }
        };
    }

    handleClickBurger() {
        this.setState({
            mobileNav: {
                active: !this.state.mobileNav.active
            }
        });
    }

    render() {
        return (
            <React.Fragment>
                {/*<Responsive minWidth={768} className={`is-desktop`}>*/}
                {/*    <ScrollspyNav*/}
                {/*        scrollTargetIds={['home', 'tours', 'contacts']}*/}
                {/*        activeNavClass="active"*/}
                {/*        scrollDuration="400"*/}
                {/*    >*/}
                {/*        <Menu pointing secondary className={`scroll-spy-nav`}>*/}
                {/*            <Menu.Item header className={`midday-title`}>*/}
                {/*                <a href="/#">MIDDAY</a>*/}
                {/*            </Menu.Item>*/}
                {/*            <Menu.Item name='главная' href="#home" active/>*/}
                {/*            <Menu.Item name='концерты' href="#tours"/>*/}
                {/*            /!*<Menu.Item name='концерты' href="#tours"/>*!/*/}
                {/*            /!*<Menu.Item name='состав' href="#band"/>*!/*/}
                {/*            <Menu.Item name='контакты' href="#contacts"/>*/}
                {/*        </Menu>*/}
                {/*    </ScrollspyNav>*/}
                {/*</Responsive>*/}
            </React.Fragment>
        );
    }
}

export default Navigation;
