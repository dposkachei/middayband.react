import React, {Component, Suspense} from 'react';
import {Redirect, Route, Switch} from 'react-router-dom';

import routes from '../../routes';
import {Button, Container, Grid, Header, Icon, Input, Dimmer, Loader} from "semantic-ui-react";
import ScrollToTop from "react-scroll-up";
import * as _ from 'lodash';
import {ToastContainer, toast} from 'react-toastify';
import APIUtils from '../../api/APIUtils.js';
import {fetchData} from "../../store/reducers/fetch";
import store from "../../store";
import VkService from "../../plugins/Vk";

const Navigation = React.lazy(() => import('./Navigation'));

class DefaultLayout extends Component {
    constructor(props) {
        super(props);
        this.state = {
            subscribe: {
                loading: false,
                email: '',
            },
            loading: true,
            user: null,
            socials: null,
        };
        this.sendSubscribe = this.sendSubscribe.bind(this);
        this.handleChange = this.handleChange.bind(this);

        store.subscribe(() => {
            this.setState({
                loading: store.getState().loading,
                socials: store.getState().socials,
            });
        });
    }

    store = () => {
        return store.getState();
    };

    initVk() {
        const interval = setInterval(() => {
            console.log(document.getElementById('vk_groups'));
            if (document.getElementById('vk_groups')) {
                VkService.group().run();
                clearInterval(interval);
            }
        }, 500);
    }

    componentDidMount() {
        fetchData();
        //this.initVk();
    }

    setStateMerge(name, object) {
        let item = _.get(this.state, name);
        _.merge(item, object);
        item = _.set({}, name, item);
        this.setState(item);
    }

    sendSubscribe(e) {
        const self = this;
        console.log(this.state);
        this.setStateMerge('subscribe', {
            loading: true
        });
        (new APIUtils('subscribe')).store({
            email: this.state.subscribe.email,
        }).then(() => {
            self.setStateMerge('subscribe', {
                loading: false,
            });
            toast("🦄 Спасибо за подписку!");
        }).catch((error) => {
            const data = error.response.data;
            if (data.message) {
                toast.error(data.message);
            }
            self.setStateMerge('subscribe', {
                loading: false
            });
        });
    }

    handleChange(event) {
        const name = event.target.name;
        const value = event.target.value;
        this.setStateMerge('subscribe', {
            [name]: value
        });

    }

    loading = () => (
        <div>
            <Dimmer active inverted>
                <Loader size='large'/>
            </Dimmer>
        </div>
    );

    render() {
        const {loading} = this.state;
        const {socials} = this.state;
        return (
            <div className={`app ${loading ? 'loading' : ''}`}>
                {loading &&
                this.loading()
                }
                <div className={`${loading ? 'hidden' : ''}`}>
                    <Container>
                        <Navigation location={this.props.location}/>
                    </Container>
                    <Suspense fallback={this.loading()}>
                        <Switch>
                            {routes.map((route, idx) => {
                                return route.component ? (
                                    <Route
                                        key={idx}
                                        path={route.path}
                                        exact={route.exact}
                                        name={route.name}
                                        render={props => (
                                            <route.component
                                                {...props}
                                            />
                                        )}
                                    />
                                ) : null;
                            })}
                            <Redirect from="/" to="/"/>
                        </Switch>
                    </Suspense>
                    <footer>
                        {/*<Container>*/}
                        {/*    <Grid centered columns={3}>*/}
                        {/*        <Grid.Row>*/}
                        {/*            <Grid.Column tablet={16} computer={4}>*/}
                        {/*                <Header as='h4' color='red' className={`__mobile-center`}>*/}
                        {/*                    Социальные сети*/}
                        {/*                </Header>*/}
                        {/*                /!*<Container className={`socials-container`}>*!/*/}
                        {/*                /!*    {_.isArray(socials) &&*!/*/}
                        {/*                /!*    socials.map((social, key) =>*!/*/}
                        {/*                /!*        <Button color={social.color} icon circular key={key}*!/*/}
                        {/*                /!*                onClick={() => {*!/*/}
                        {/*                /!*                    window.open(social.url, '_blank').focus()*!/*/}
                        {/*                /!*                }}*!/*/}
                        {/*                /!*        >*!/*/}
                        {/*                /!*            <Icon name={social.name}/>*!/*/}
                        {/*                /!*        </Button>*!/*/}
                        {/*                /!*    )*!/*/}
                        {/*                /!*    }*!/*/}
                        {/*                /!*</Container>*!/*/}
                        {/*                <Container className={`socials-container __mobile-center`}>*/}
                        {/*                    {_.isArray(socials) &&*/}
                        {/*                    socials.map((social, key) =>*/}
                        {/*                        <Button icon key={key}*/}
                        {/*                                onClick={() => {*/}
                        {/*                                    window.open(social.url, '_blank').focus()*/}
                        {/*                                }}*/}
                        {/*                        >*/}
                        {/*                            <Icon name={social.name}/>*/}
                        {/*                        </Button>*/}
                        {/*                    )*/}
                        {/*                    }*/}
                        {/*                </Container>*/}
                        {/*            </Grid.Column>*/}
                        {/*            <Grid.Column tablet={16} computer={4}>*/}
                        {/*                <Header as='h4' color='red' className={`__mobile-center`}>*/}
                        {/*                    Официальная группа*/}
                        {/*                </Header>*/}
                        {/*                <Container className={`vk-container __mobile-center`}>*/}
                        {/*                    <div id="vk_groups"></div>*/}
                        {/*                </Container>*/}
                        {/*            </Grid.Column>*/}
                        {/*            <Grid.Column tablet={16} computer={4}>*/}
                        {/*                <Header as='h4' color='red' className={`__mobile-center`}>*/}
                        {/*                    Подписаться*/}
                        {/*                </Header>*/}
                        {/*                <p className={`__mobile-center`}>*/}
                        {/*                    Вы можете подписаться на рассылу новостей связанные с группой.*/}
                        {/*                    Это может быть информация о предстоящий релизах, новых мероприятиях и других интересных событий.*/}
                        {/*                </p>*/}
                        {/*                <Input name={`email`} icon='mail' iconPosition='left' placeholder='Введите свой email' className={`__mobile-full`} action={*/}
                        {/*                    <Button color={`red`} onClick={this.sendSubscribe} className={this.state.subscribe.loading ? 'is-loading' : ''}>Отправить</Button>*/}
                        {/*                } onChange={this.handleChange}/>*/}
                        {/*            </Grid.Column>*/}
                        {/*        </Grid.Row>*/}
                        {/*    </Grid>*/}
                        {/*</Container>*/}
                        <Container textAlign='center'>
                            ALL MATERIAL © MIDDAY 2019 // ALL RIGHTS RESERVED
                        </Container>
                    </footer>
                    <ScrollToTop showUnder={160} style={{right: 54, zIndex: 101}}>
                        <Button circular icon='angle up'/>
                    </ScrollToTop>
                    <ToastContainer/>
                </div>
            </div>
        );
    }
}

export default DefaultLayout;
