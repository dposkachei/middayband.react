import React from 'react';
import { Route, Switch, BrowserRouter } from 'react-router-dom';
import './App.scss';
import {Dimmer, Loader} from "semantic-ui-react";

const loading = () => (
    <div>
        <Dimmer active inverted>
            <Loader size='large'/>
        </Dimmer>
    </div>
);

// Containers
const DefaultLayout = React.lazy(() => import('./containers/DefaultLayout'));

function App() {
    return (
        <BrowserRouter>
            <React.Suspense fallback={loading()}>
                <Switch>
                    <Route
                        path="/"
                        name="Home"
                        render={props => <DefaultLayout {...props} />}
                    />
                </Switch>
            </React.Suspense>
        </BrowserRouter>
    );
}

export default App;
