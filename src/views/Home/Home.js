import React, {Component} from 'react';

import {Image, Header, Segment} from "semantic-ui-react";

//import band from "../../assets/img/band.jpg";
import band2 from "../../assets/img/band2.jpg";
//import band3 from "../../assets/img/band 2.jpg";
//import full from "../../assets/img/_MG_0734.jpg";
import bg from "../../assets/img/chinese_ink_painting_bg.png";
import store from "../../store";
//import * as _ from "lodash";

//const Members = React.lazy(() => import('./../Base/Members'));

//const Album = React.lazy(() => import('./../Base/Album'));
const Main = React.lazy(() => import('./../Base/Main'));
const Tours = React.lazy(() => import('./../Base/Tours'));
//const Tours = React.lazy(() => import('./../Base/Tours'));

class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalIsOpen: false,
            rating: 1,
            maxRating: 5,
            email: '',
            images: {},
            blocks: {},
        };

        store.subscribe(() => {
            this.setState({
                images: store.getState().images,
                blocks: store.getState().blocks,
            });
        });
    }

    handleRate = (e, {rating, maxRating}) =>
        this.setState({rating, maxRating});

    toggleModal = () => {
        this.setState(state => ({modalIsOpen: !state.modalIsOpen}));
    };

    render() {
        //const {images} = this.state;
        const {blocks} = this.state;
        return (
            <React.Fragment>
                <div className={`bg-main`} style={{backgroundImage: `url(${bg})`}}>

                </div>
                <div id={`home`}>
                    <Main/>

                    {/*<Image src={full} fluid/>*/}
                    {/*/!*{images.main !== undefined &&*!/*/}
                    {/*/!*    <Image src={images.main} fluid />*!/*/}
                    {/*/!*}*!/*/}

                    {/*<div className="strip-outer midday-outer">*/}
                    {/*    <div className="strip-cover midday-cover">*/}
                    {/*        <h2 className="strip-cover__title midday-title">*/}
                    {/*            <span className={`midday-m`}>M</span>*/}
                    {/*            <span className={`midday-i`}>I</span>*/}
                    {/*            <span className={`midday-d-1`}>D</span>*/}
                    {/*            <span className={`midday-d-2`}>D</span>*/}
                    {/*            <span className={`midday-a`}>A</span>*/}
                    {/*            <span className={`midday-y`}>Y</span>*/}
                    {/*        </h2>*/}
                    {/*    </div>*/}
                    {/*</div>*/}
                </div>
                {/*<div id={`music`}>*/}
                {/*    {blocks.music &&*/}
                {/*    <Segment massive='true' textAlign='center' padded='very' color={`red`} basic>*/}
                {/*        <Header as='h2'>{blocks.music.title}</Header>*/}
                {/*        <p>{blocks.music.description}</p>*/}
                {/*    </Segment>*/}
                {/*    }*/}
                {/*    <Album/>*/}
                {/*</div>*/}
                {/*<div id={`tours`}>*/}
                {/*    {blocks.tours &&*/}
                {/*    <Segment massive='true' textAlign='center' padded='very' color={`red`} basic>*/}
                {/*        <Header as='h2'>{blocks.tours.title}</Header>*/}
                {/*        <p>{blocks.tours.description}</p>*/}
                {/*    </Segment>*/}
                {/*    }*/}
                {/*    <Tours/>*/}
                {/*</div>*/}
                {/*<div id={`band`}>*/}
                {/*    {blocks.band &&*/}
                {/*    <Segment massive='true' textAlign='center' padded='very' color={`red`} basic>*/}
                {/*        <Header as='h2'>{blocks.band.title}</Header>*/}
                {/*        <p>{blocks.band.description}</p>*/}
                {/*    </Segment>*/}
                {/*    }*/}
                {/*    <Members/>*/}
                {/*</div>*/}
                {/*<div id={`contacts`}>*/}
                {/*    {blocks.contacts &&*/}
                {/*    <Segment massive='true' textAlign='center' padded='very' color={`red`} basic>*/}
                {/*        <Header as='h2'>{blocks.contacts.title}</Header>*/}
                {/*        <p>{blocks.contacts.description}</p>*/}
                {/*    </Segment>*/}
                {/*    }*/}
                {/*    <Image src={band2} fluid/>*/}
                {/*    /!*{images.footer !== undefined &&*!/*/}
                {/*    /!*    <Image src={images.footer} fluid />*!/*/}
                {/*    /!*}*!/*/}

                {/*</div>*/}
            </React.Fragment>
        );
    }
}

export default Home;
