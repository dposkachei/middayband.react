import React, {Component} from 'react';
import bg from "../../assets/img/chinese_ink_painting_bg.png";
import {Container, Dimmer, Loader} from "semantic-ui-react";
import {fetchData, getDataRelease} from "../../store/reducers/Data";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";

import Release from './Container/Release';

class Index extends Component {
    _isMounted = false;
    state = {};

    constructor(props) {
        super(props);
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
    }

    componentDidMount() {
        const {fetchData} = this.props;
        this._isMounted = true;
        fetchData();
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        const {release} = this.props;
        return this._isMounted && release.title !== undefined;
    }

    loading = () => (
        <div>
            <Dimmer active inverted>
                <Loader size='large'/>
            </Dimmer>
        </div>
    );

    render() {
        const {release} = this.props;
        if (!this.shouldComponentRender()) return (this.loading());
        return (
            <React.Fragment>
                <div className={`bg-main`} style={{backgroundImage: `url(${bg})`}}/>
                <div id={`home`}>
                    <Release release={release}/>
                </div>
                <footer>
                    <Container textAlign='center'>
                        ALL MATERIAL © MIDDAY 2020 // ALL RIGHTS RESERVED
                    </Container>
                </footer>
            </React.Fragment>
        );
    }
}


const mapStateToProps = state => ({
    release: getDataRelease(state),
});
const mapDispatchToProps = dispatch => bindActionCreators({
    fetchData: fetchData
}, dispatch);

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Index);
