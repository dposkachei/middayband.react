import React, {Component} from 'react';
import {Container, Grid, Header, Image, Segment, Responsive, Button, Icon, Popup} from "semantic-ui-react";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import * as _ from "lodash";
import VkService from "../../../plugins/Vk";

class Release extends Component {
    _isMounted = false;
    state = {};

    constructor(props) {
        super(props);
        this.state = {
            release: props.release || {},
        };
        if (props.release.vk_playlist !== null) {
            VkService.playlist(props.release.vk_playlist, props.release.vk_owner_id, props.release.vk_hash).run();
        }
        this.shouldComponentRender = this.shouldComponentRender.bind(this);
    }

    componentDidMount() {
        this._isMounted = true;
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    shouldComponentRender() {
        return this._isMounted;
    }

    render() {
        const {release} = this.state;
        if (!release) return ('');
        return (
            <React.Fragment>
                <Segment massive='true' padded='very' basic className={`block-section`}>
                    <Container>
                        <Grid columns={2} relaxed='very' stackable>
                            <Grid.Column className={``} only='mobile'>
                                <div>
                                    <div className="strip-cover midday-cover">
                                        <h2 className="strip-cover__title midday-title">
                                            <span className={`midday-m`}>M</span>
                                            <span className={`midday-i`}>I</span>
                                            <span className={`midday-d-1`}>D</span>
                                            <span className={`midday-d-2`}>D</span>
                                            <span className={`midday-a`}>A</span>
                                            <span className={`midday-y`}>Y</span>
                                        </h2>
                                    </div>
                                    <Segment massive='true' padded basic>
                                        <Container textAlign='center'>
                                            {release.description}
                                        </Container>
                                    </Segment>
                                </div>
                            </Grid.Column>
                            <Grid.Column>
                                <Segment raised style={{padding: 0, borderRadius: 0, border: 'none'}}>
                                    <Image src={release.image} fluid/>
                                </Segment>
                                <Header as='h3' textAlign='center'>
                                    {release.title} [{release.year}]
                                </Header>
                                {/*<Container textAlign='center'>*/}
                                {/*    <Rating maxRating={5} clearable onRate={this.handleRate} rating={this.state.rating}/>*/}
                                {/*</Container>*/}
                                <Container textAlign='center'>
                                    {release.label}
                                </Container>
                                <Segment vertical textAlign='center' className={`socials-container`}>
                                    {/*{_.isArray(socials) &&*/}
                                    {/*socials.map((social, key) =>*/}
                                    {/*    <Button icon key={key}*/}
                                    {/*            onClick={() => {*/}
                                    {/*                window.open(social.url, '_blank').focus()*/}
                                    {/*            }}*/}
                                    {/*    >*/}
                                    {/*        <Icon name={social.name}/>*/}
                                    {/*    </Button>*/}
                                    {/*)*/}
                                    {/*}*/}


                                    {release !== undefined && _.isArray(release.links) &&
                                    release.links.map((link, key) =>
                                        <Popup key={key} content={`Перейти по ссылке в ` + link.title} basic size='tiny' trigger={
                                            <Button icon
                                                    onClick={() => {
                                                        window.open(link.url, '_blank').focus()
                                                    }}
                                            >
                                                <Icon name={link.icon}/>
                                            </Button>
                                        }/>
                                    )
                                    }
                                </Segment>
                            </Grid.Column>
                            <Grid.Column className={``}>
                                <Responsive minWidth={Responsive.onlyTablet.minWidth}>
                                    <div className="strip-cover midday-cover">
                                        <h2 className="strip-cover__title midday-title">
                                            <span className={`midday-m`}>M</span>
                                            <span className={`midday-i`}>I</span>
                                            <span className={`midday-d-1`}>D</span>
                                            <span className={`midday-d-2`}>D</span>
                                            <span className={`midday-a`}>A</span>
                                            <span className={`midday-y`}>Y</span>
                                        </h2>
                                    </div>
                                    <Segment massive='true' padded='very' basic>
                                        <Container textAlign='center'>
                                            {release.description}
                                        </Container>
                                    </Segment>
                                </Responsive>
                                {release.vk_playlist !== null &&
                                <Segment raised className={`segment-vk`}>
                                    <div id="vk_playlist_-2000688606_3688606"></div>
                                </Segment>
                                }
                                {release.vk_playlist === null &&
                                <Segment massive='true' padded='very'>
                                    <Container textAlign='center'>
                                        Плейлист из &#160;<b><code>https://vk.com</code></b>&#160; скоро будет ...
                                    </Container>
                                </Segment>
                                }
                            </Grid.Column>
                        </Grid>
                    </Container>
                </Segment>
            </React.Fragment>
        );
    };
}

const mapStateToProps = state => ({});
const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Release);
