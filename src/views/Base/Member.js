import React, {Component} from 'react';
import {Header, Image, Grid, Label} from "semantic-ui-react";

class Member extends Component {
    constructor(props) {
        super(props);
        this.state = {
            member: this.props.member,
        };
    }

    render() {
        const {member} = this.state;
        return (
            <React.Fragment>
                <Grid.Row columns={2}>
                    <Grid.Column mobile={16} tablet={8} computer={3} className={`__member-image`}>
                        <Image src={member.image} />
                    </Grid.Column>
                    <Grid.Column mobile={16} tablet={8} computer={13} className={`__member-info`}>
                        <Header
                            as='h2'
                            content={member.title}
                            subheader={member.role}
                        />
                        <p>
                            {member.description}
                        </p>
                        {member.size !== '' && member.size !== null &&
                            <Label>{member.size}</Label>
                        }
                    </Grid.Column>
                </Grid.Row>
            </React.Fragment>
        );
    }
}

export default Member;
