import React, {Component} from 'react';
import {Header, Image, Grid, Divider, Modal, Button} from "semantic-ui-react";
import Carousel, {Modal as ModalImage, ModalGateway} from "react-images";

const Tickets = React.lazy(() => import('./../Base/Tickets'));

class Member extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalIsOpen: false,
            concert: this.props.concert,
        };
    }

    render() {
        const {modalIsOpen} = this.state;
        const {concert} = this.state;
        return (
            <React.Fragment>
                <Divider horizontal>{concert.begin_at_format}</Divider>
                <div>
                    <Grid columns={3} relaxed='very' stackable>
                        <Grid.Column mobile={16} tablet={16} computer={6}>
                            <Grid columns={2}>
                                <Grid.Column mobile={16} tablet={16} computer={4} >
                                    <Image
                                        src={concert.image} size='small' className={`__mobile-center __mobile-full`}
                                    />
                                </Grid.Column>
                                <Grid.Column mobile={16} tablet={16} computer={12} className={`__mobile-center`}>
                                    <p>{concert.title}</p>
                                    {/*<Rating maxRating={5} clearable onRate={this.handleRate} rating={this.state.rating}/>*/}
                                </Grid.Column>
                            </Grid>
                        </Grid.Column>
                        <Grid.Column verticalAlign='middle' mobile={16} tablet={16} computer={4}>
                            <Header as='h3' textAlign='center'>
                                {concert.area.place}
                            </Header>
                        </Grid.Column>
                        <Grid.Column verticalAlign='middle' textAlign='right' mobile={16} tablet={16} computer={6}>
                            {/*<TransitionModal*/}
                            {/*    animation="fade up"*/}
                            {/*    duration={250}*/}
                            {/*    trigger={(*/}
                            {/*        <Button secondary>Билеты</Button>*/}
                            {/*    )}*/}
                            {/*    closeIcon*/}
                            {/*>*/}
                            {/*    <Tickets/>*/}
                            {/*</TransitionModal>*/}
                            <Modal trigger={<Button secondary className={`__mobile-full`}>Билеты</Button>} closeIcon onOpen={this.afterOpenModal}>
                                <Tickets concert={concert}/>
                            </Modal>
                        </Grid.Column>
                    </Grid>
                </div>
            </React.Fragment>
        );
    }
}

export default Member;
