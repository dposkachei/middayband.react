import React, {Component} from 'react';
import {
    Container,
    Segment,
    Divider
} from "semantic-ui-react";

import store from "../../store";
import {animateScroll as scroll} from 'react-scroll';
import * as _ from "lodash";

const Concert = React.lazy(() => import('./../Base/Concert'));

class Tours extends Component {
    constructor(props) {
        super(props);
        this.state = {
            rating: 1,
            maxRating: 5,
            blocks: {},
            concerts: {},
        };

        store.subscribe(() => {
            this.setState({
                blocks: store.getState().blocks,
                concerts: store.getState().concerts,
            });
        });
    }

    handleRate = (e, {rating, maxRating}) =>
        this.setState({rating, maxRating});

    toggleModal = () => {
        this.setState(state => ({modalIsOpen: !state.modalIsOpen}));
    };
    afterOpenModal() {
        setTimeout(function () {
            scroll.scrollTo(document.getElementById('tours').offsetTop, {
                duration: 0
            });
        }, 100);
    }

    render() {
        const {blocks} = this.state;
        const {concerts} = this.state;
        return (
            <React.Fragment>
                <Segment massive='true' padded basic>
                    <Container>
                        {_.isArray(concerts) &&
                        concerts.map((concert, key) =>
                            <Concert concert={concert} key={key}/>
                        )
                        }
                        <Divider horizontal>...</Divider>
                        {blocks.tours &&
                        <Segment textAlign={`center`} basic>
                            {blocks.tours.purpose}
                        </Segment>
                        }
                    </Container>
                </Segment>
            </React.Fragment>
        );
    };
}

export default Tours;
