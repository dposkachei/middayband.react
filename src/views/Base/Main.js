import React, {Component} from 'react';
import {Container, Grid, Header, Image, Label, Segment, Responsive, Button, Icon, Popup} from "semantic-ui-react";

import VkService from "../../plugins/Vk";
import store from "../../store";
import * as _ from "lodash";

class Album extends Component {
    constructor(props) {
        super(props);
        this.state = {
            release: {},
            modalIsOpen: false,
            rating: 1,
            maxRating: 5,
            havePlaylist: false,
            socials: [],
        };
        store.subscribe(() => {
            this.setState({
                release: store.getState().release,
                socials: store.getState().socials,
            });
            if (this.state.release.vk_playlist !== undefined && !this.state.havePlaylist) {
                VkService.playlist(this.state.release.vk_playlist, this.state.release.vk_owner_id).run();
                this.setState({
                    havePlaylist: true,
                });

            }
        });
    }

    handleRate = (e, {rating, maxRating}) =>
        this.setState({rating, maxRating});

    render() {
        const {release} = this.state;
        const {socials} = this.state;
        return (
            <React.Fragment>
                <Segment massive='true' padded='very' basic className={`block-section`}>
                    <Container>
                        <Grid columns={2} relaxed='very' stackable>
                            <Grid.Column className={``} only='mobile'>
                                <div>
                                    <div className="strip-cover midday-cover">
                                        <h2 className="strip-cover__title midday-title">
                                            <span className={`midday-m`}>M</span>
                                            <span className={`midday-i`}>I</span>
                                            <span className={`midday-d-1`}>D</span>
                                            <span className={`midday-d-2`}>D</span>
                                            <span className={`midday-a`}>A</span>
                                            <span className={`midday-y`}>Y</span>
                                        </h2>
                                    </div>
                                    <Segment massive='true' padded basic>
                                        <Container textAlign='center'>
                                            {release.description}
                                        </Container>
                                    </Segment>
                                </div>
                            </Grid.Column>
                            <Grid.Column>
                                <Segment raised>
                                    <Image src={release.image} fluid bordered/>
                                </Segment>
                                <Header as='h3' textAlign='center'>
                                    {release.title} [{release.year}]
                                </Header>
                                {/*<Container textAlign='center'>*/}
                                {/*    <Rating maxRating={5} clearable onRate={this.handleRate} rating={this.state.rating}/>*/}
                                {/*</Container>*/}
                                <Container textAlign='center'>
                                    {release.description}
                                </Container>
                                <Segment vertical textAlign='center' className={`socials-container`}>
                                    {/*{_.isArray(socials) &&*/}
                                    {/*socials.map((social, key) =>*/}
                                    {/*    <Button icon key={key}*/}
                                    {/*            onClick={() => {*/}
                                    {/*                window.open(social.url, '_blank').focus()*/}
                                    {/*            }}*/}
                                    {/*    >*/}
                                    {/*        <Icon name={social.name}/>*/}
                                    {/*    </Button>*/}
                                    {/*)*/}
                                    {/*}*/}


                                    {release !== undefined && _.isArray(release.links) &&
                                    release.links.map((link, key) =>
                                        <Popup key={key} content={link.title} basic size='tiny' trigger={
                                            <Button icon
                                                    onClick={() => {
                                                        window.open(link.url, '_blank').focus()
                                                    }}
                                            >
                                                <Icon name={link.icon}/>
                                            </Button>
                                        } />
                                    )
                                    }
                                </Segment>
                            </Grid.Column>
                            <Grid.Column className={``}>
                                <Responsive minWidth={Responsive.onlyTablet.minWidth}>
                                    <div className="strip-cover midday-cover">
                                        <h2 className="strip-cover__title midday-title">
                                            <span className={`midday-m`}>M</span>
                                            <span className={`midday-i`}>I</span>
                                            <span className={`midday-d-1`}>D</span>
                                            <span className={`midday-d-2`}>D</span>
                                            <span className={`midday-a`}>A</span>
                                            <span className={`midday-y`}>Y</span>
                                        </h2>
                                    </div>
                                    <Segment massive='true' padded='very' basic>
                                        <Container textAlign='center'>
                                            {release.description}
                                        </Container>
                                    </Segment>
                                </Responsive>
                                <Segment raised className={`segment-vk ${!this.state.havePlaylist ? 'loading' : ''}`}>
                                    <div id="vk_playlist_-2000688606_3688606"></div>
                                </Segment>
                            </Grid.Column>
                        </Grid>
                    </Container>
                </Segment>
            </React.Fragment>
        );
    };
}

export default Album;
