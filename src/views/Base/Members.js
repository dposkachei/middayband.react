import React, {Component} from 'react';

import {Container, Segment, Grid} from "semantic-ui-react";
import store from "../../store";
import * as _ from 'lodash';
const Member = React.lazy(() => import('./Member'));

class Members extends Component {
    constructor(props) {
        super(props);
        this.state = {
            members: {}
        };
        store.subscribe(() => {
            this.setState({
                members: store.getState().members
            });
        });
    }

    store = () => {
        return store.getState();
    };

    render() {
        const {members} = this.state;

        return (
            <React.Fragment>
                <Segment massive='true' padded='very' basic className={`block-section`}>
                    <Container>
                        <Grid>
                            {_.isArray(members) &&
                                members.map((member, key) =>
                                    <Member member={member} key={key}/>
                                )
                            }
                        </Grid>
                    </Container>
                </Segment>
            </React.Fragment>
        );
    }
}

export default Members;
