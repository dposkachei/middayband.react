import React, {Component} from 'react';
import {Container, Grid, Header, Icon, Modal, Table, Divider} from "semantic-ui-react";

import VkService from "../../plugins/Vk";

class Tickets extends Component {
    constructor(props) {
        super(props);
        this.state = {
            concert: this.props.concert,
        };
    }
    componentDidMount() {
        VkService.event(this.state.concert.vk_event).run();
    }

    render() {
        const {concert} = this.state;
        return (
            <React.Fragment>
                <Modal.Content image>
                    <Modal.Description>
                        <Container>
                            <Grid columns={2} relaxed='very' stackable>
                                <Grid.Column width={3}>
                                    <div className={`shadow-inset`}>
                                        <div id="vk_event"></div>
                                    </div>
                                </Grid.Column>
                                <Grid.Column width={9}>
                                    <Header as='h3' textAlign='center'>
                                        {concert.title}
                                    </Header>
                                    {concert.support !== null &&
                                        <p className={`center`}>
                                            Специальные гости: {concert.support}
                                        </p>
                                    }
                                    <Divider horizontal>
                                        <Header as='h4'>
                                            <Icon name='tag'/>
                                            Описание
                                        </Header>
                                    </Divider>
                                    <p>
                                        {concert.description}
                                    </p>
                                    <Divider horizontal>
                                        <Header as='h4'>
                                            <Icon name='bar chart'/>
                                            Цена
                                        </Header>
                                    </Divider>

                                    <Table definition>
                                        <Table.Body>
                                            {/*<Table.Row>*/}
                                            {/*    <Table.Cell width={3}><Icon name='ruble'/> 200.00</Table.Cell>*/}
                                            {/*    <Table.Cell>Во встречи в VK</Table.Cell>*/}
                                            {/*</Table.Row>*/}
                                            <Table.Row>
                                                <Table.Cell width={3}><Icon name='ruble'/> 200.00</Table.Cell>
                                                <Table.Cell>С репостом (показать репост на входе)</Table.Cell>
                                            </Table.Row>
                                            <Table.Row>
                                                <Table.Cell><Icon name='ruble'/> {concert.price}.00</Table.Cell>
                                                <Table.Cell>Без репоста</Table.Cell>
                                            </Table.Row>
                                        </Table.Body>
                                    </Table>

                                    <Divider horizontal>...</Divider>
                                    <p>
                                        Доп. информация по тел.: {concert.area.phone} <br/>
                                        Время: 19:00 <br/>
                                        Адрес: {concert.area.address}
                                    </p>
                                </Grid.Column>
                            </Grid>
                        </Container>
                    </Modal.Description>
                </Modal.Content>
            </React.Fragment>
        );
    }
}

export default Tickets;
