import React, {Component} from 'react';
import {Container, Grid, Header, Image, Label, Segment} from "semantic-ui-react";

import VkService from "../../plugins/Vk";
import store from "../../store";
import * as _ from "lodash";

class Album extends Component {
    constructor(props) {
        super(props);
        this.state = {
            release: {},
            modalIsOpen: false,
            rating: 1,
            maxRating: 5,
            havePlaylist: false,
        };
        store.subscribe(() => {
            this.setState({
                release: store.getState().release
            });
            if (this.state.release.vk_playlist !== undefined && !this.state.havePlaylist) {
                VkService.playlist(this.state.release.vk_playlist, this.state.release.vk_owner_id).run();
                this.setState({
                    havePlaylist: true,
                });

            }
        });
    }

    handleRate = (e, {rating, maxRating}) =>
        this.setState({rating, maxRating});

    render() {
        const {release} = this.state;
        return (
            <React.Fragment>
                <Segment massive='true' padded='very' basic className={`block-section`}>
                    <Container>
                        <Grid columns={2} relaxed='very' stackable>
                            <Grid.Column>
                                <Segment raised>
                                    <Image src={release.image} fluid bordered/>
                                </Segment>
                                <Header as='h3' textAlign='center'>
                                    {release.title} [{release.year}]
                                </Header>
                                {/*<Container textAlign='center'>*/}
                                {/*    <Rating maxRating={5} clearable onRate={this.handleRate} rating={this.state.rating}/>*/}
                                {/*</Container>*/}
                                <Container textAlign='center'>
                                    {release.description}
                                </Container>
                                <Segment vertical textAlign='center'>
                                    {release !== undefined && _.isArray(release.links) &&
                                    release.links.map((link, key) =>
                                        <Label as='a' content={link.title} key={key} basic
                                               onClick={() => {
                                                   window.open(link.url, '_blank').focus()
                                               }}
                                        />
                                    )
                                    }
                                </Segment>
                            </Grid.Column>
                            <Grid.Column>
                                <Segment raised className={`segment-vk ${!this.state.havePlaylist ? 'loading' : ''}`}>
                                    <div id="vk_playlist_-2000688606_3688606"></div>
                                </Segment>
                            </Grid.Column>
                        </Grid>
                    </Container>
                </Segment>
            </React.Fragment>
        );
    };
}

export default Album;
