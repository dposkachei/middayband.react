const axios = require('axios');

export default class APIUtils {

    host;
    url;
    basicAuth;

    constructor(url = null) {
        this.host = 'https://middayband.ru/api';
        this.url = url;
        this.basicAuth = 'Basic c3R1ZHlxYTpGU01SeVRNcVFiN2F6QVFM';
    }

    index(parameters = {}) {
        console.log(parameters);
        return axios.get(`${this.host}/${this.url}`, {
            params: parameters,
            headers: { 'Authorization': this.basicAuth }
        });
    };

    store(parameters) {
        return axios.post(`${this.host}/${this.url}`, parameters, {
            headers: { 'Authorization': this.basicAuth }
        });
    }

    show(id) {
        return axios.get(`${this.host}/${this.url}/${id}`, {
            headers: { 'Authorization': this.basicAuth }
        });
    };

    update(id, parameters) {
        return axios.post(`${this.host}/${this.url}/${id}`, parameters, {
            headers: { 'Authorization': this.basicAuth }
        });
    }

    destroy(id) {
        return axios.post(`${this.host}/${this.url}/${id}/destroy`, {}, {
            headers: { 'Authorization': this.basicAuth }
        });
    }

    login(parameters) {
        return axios.post(`${this.host}/login`, parameters, {
            headers: { 'Authorization': this.basicAuth }
        });
    }

    authCheck(hash) {
        return axios.post(`${this.host}/user`, {
            hash: hash
        }, {
            headers: { 'Authorization': this.basicAuth }
        });
    }
};
