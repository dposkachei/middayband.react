import { createStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk'
import { composeWithDevTools  } from 'redux-devtools-extension';
import { combineReducers } from 'redux';
import progress from 'react-redux-progress/reducer';
import {releaseReducer} from './reducers/Release/index';
import {dataReducer} from './reducers/Data/index';

// Root Reducer
const rootReducer = combineReducers({
    release: releaseReducer,
    data: dataReducer,
});
const rootReducers = combineReducers({
    progress,
    rootReducer,
    // other reducers
});
const middlewareEnhancer = applyMiddleware(thunkMiddleware);

const store = createStore(rootReducers, composeWithDevTools(middlewareEnhancer));

export default store;
