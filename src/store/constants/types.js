export const ADD_DATA = "ADD_DATA";
export const ADD_RELEASE = "ADD_RELEASE";
export const SET_LOADING = "SET_LOADING";
export const ADD_SOCIALS = "ADD_SOCIALS";
export const ADD_IMAGES = "ADD_IMAGES";
export const ADD_BLOCKS = "ADD_BLOCKS";
export const SET_NAVIGATION = "SET_NAVIGATION";
export const ADD_CONCERTS = "ADD_CONCERTS";
