import APIUtils from "../../api/APIUtils";
import store from "../../store/index";
import {addData, addRelease, setLoading, addSocials, addImages, addBlocks, addConcerts, setNavigation} from "../actions";
import {animateScroll as scroll} from "react-scroll";

function fetchData() {
    (new APIUtils('data')).index().then(res => {
        const data = res.data.data;
        console.log(data);
        store.dispatch(addData(data.members));
        store.dispatch(addRelease(data.release));
        store.dispatch(addSocials(data.socials));
        store.dispatch(addImages(data.images));
        store.dispatch(addBlocks(data.blocks));
        store.dispatch(addConcerts(data.concerts));
        fetchLoading(false);
    });
}
function fetchLoading(loading) {
    store.dispatch(setLoading(loading));
}
function fetchNavigation(name) {
    scroll.scrollTo(document.getElementById(name).offsetTop);
    store.dispatch(setNavigation(name));
}
export {
    fetchData,
    fetchLoading,
    fetchNavigation,
};
