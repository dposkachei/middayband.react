import {
    ADD_DATA,
    SET_LOADING,
    ADD_RELEASE,
    ADD_SOCIALS,
    ADD_IMAGES,
    ADD_BLOCKS,
    ADD_CONCERTS,
    SET_NAVIGATION,
} from "../constants/types";

const initialState = {
    loading: true,
    members: [],
    release: {},
    socials: [],
    images: {},
    blocks: {},
    concerts: {},
    navigationActive: 'home',
};

function rootReducer(state = initialState, action) {
    if (action.type === ADD_DATA) {
        return Object.assign({}, state, {
            members: state.members.concat(action.payload)
        });
    }
    if (action.type === ADD_SOCIALS) {
        return Object.assign({}, state, {
            socials: action.payload
        });
    }
    if (action.type === ADD_IMAGES) {
        return Object.assign({}, state, {
            images: action.payload
        });
    }
    if (action.type === ADD_BLOCKS) {
        return Object.assign({}, state, {
            blocks: action.payload
        });
    }
    if (action.type === ADD_RELEASE) {
        return Object.assign({}, state, {
            release: action.payload
        });
    }
    if (action.type === ADD_CONCERTS) {
        return Object.assign({}, state, {
            concerts: action.payload
        });
    }
    if (action.type === SET_LOADING) {
        return Object.assign({}, state, {
            loading: action.payload
        });
    }
    if (action.type === SET_NAVIGATION) {
        return Object.assign({}, state, {
            navigationActive: action.payload
        });
    }
    return state;
}

export default rootReducer;
