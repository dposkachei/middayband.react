import {API_URL} from "../../../api/api";

const FETCH_RELEASE_PENDING = 'FETCH_RELEASE_PENDING';
const FETCH_RELEASE_SUCCESS = 'FETCH_RELEASE_SUCCESS';
const FETCH_RELEASE_ERROR = 'FETCH_RELEASE_ERROR';

const defaultState = {
    pending: false,
    release: {},
    error: null
};

function fetchReleasePending() {
    return {
        type: FETCH_RELEASE_PENDING
    }
}

function fetchReleaseSuccess(data) {
    return {
        type: FETCH_RELEASE_SUCCESS,
        release: data
    }
}

function fetchReleaseError(error) {
    return {
        type: FETCH_RELEASE_ERROR,
        error: error
    }
}

export function fetchRelease() {
    return dispatch => {
        dispatch(fetchReleasePending());
        fetch(API_URL + '/data')
            .then(res => res.json())
            .then(res => {
                if (res.error) {
                    throw(res.error);
                }
                dispatch(fetchReleaseSuccess(res.data));
                return res.data;
            })
            .catch(error => {
                dispatch(fetchReleaseError(error));
            })
    }
}

export function releaseReducer(state = defaultState, action) {
    switch (action.type) {
        case FETCH_RELEASE_PENDING:
            return {
                ...state,
                pending: true
            };
        case FETCH_RELEASE_SUCCESS:
            return {
                ...state,
                pending: false,
                release: action.release
            };
        case FETCH_RELEASE_ERROR:
            return {
                ...state,
                pending: false,
                error: action.error
            };
        default:
            return state;
    }
}

export const getRelease = state => state.rootReducer.release.release;
export const getReleasePending = state => state.rootReducer.release.pending;
export const getReleaseError = state => state.rootReducer.release.error;

//export default {fetchDirections, DirectionsReducer}
