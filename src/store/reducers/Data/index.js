import {API_URL} from "../../../api/api";

const FETCH_DATA_PENDING = 'FETCH_DATA_PENDING';
const FETCH_DATA_SUCCESS = 'FETCH_DATA_SUCCESS';
const FETCH_DATA_ERROR = 'FETCH_DATA_ERROR';

const defaultState = {
    pending: false,
    release: {},
    error: null
};

function fetchDataPending() {
    return {
        type: FETCH_DATA_PENDING
    }
}

function fetchDataSuccess(data) {
    return {
        type: FETCH_DATA_SUCCESS,
        data: data
    }
}

function fetchDataError(error) {
    return {
        type: FETCH_DATA_ERROR,
        error: error
    }
}

export function fetchData() {
    return dispatch => {
        dispatch(fetchDataPending());
        fetch(API_URL + '/data')
            .then(res => res.json())
            .then(res => {
                if (res.error) {
                    throw(res.error);
                }
                dispatch(fetchDataSuccess(res.data));
                return res.data;
            })
            .catch(error => {
                dispatch(fetchDataError(error));
            })
    }
}

export function dataReducer(state = defaultState, action) {
    switch (action.type) {
        case FETCH_DATA_PENDING:
            return {
                ...state,
                pending: true
            };
        case FETCH_DATA_SUCCESS:
            return {
                ...state,
                pending: false,
                release: action.data.release,
            };
        case FETCH_DATA_ERROR:
            return {
                ...state,
                pending: false,
                error: action.error
            };
        default:
            return state;
    }
}

export const getDataRelease = state => state.rootReducer.data.release;
export const getDataPending = state => state.rootReducer.data.pending;
export const getDataError = state => state.rootReducer.data.error;

//export default {fetchDirections, DirectionsReducer}
