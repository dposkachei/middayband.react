import {ADD_DATA, ADD_RELEASE, ADD_SOCIALS, SET_LOADING, ADD_IMAGES, ADD_BLOCKS, ADD_CONCERTS, SET_NAVIGATION} from "../constants/types";

export function addData(payload) {
    return {
        type: ADD_DATA, payload
    };
}
export function addRelease(payload) {
    return {
        type: ADD_RELEASE, payload
    };
}
export function addSocials(payload) {
    return {
        type: ADD_SOCIALS, payload
    };
}
export function addImages(payload) {
    return {
        type: ADD_IMAGES, payload
    };
}
export function addBlocks(payload) {
    return {
        type: ADD_BLOCKS, payload
    };
}
export function addConcerts(payload) {
    return {
        type: ADD_CONCERTS, payload
    };
}
export function setLoading(payload) {
    return {
        type: SET_LOADING, payload
    };
}
export function setNavigation(payload) {
    return {
        type: SET_NAVIGATION, payload
    };
}
