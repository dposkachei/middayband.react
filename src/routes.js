import React from 'react';

const Index = React.lazy(() => import('./views/Index/Index'));

// https://github.com/ReactTraining/react-router/tree/master/packages/react-router-config
const routes = [
    { path: '/', exact: true, name: 'Index', component: Index },
];

export default routes;
