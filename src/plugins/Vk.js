
const VkService = {

    function: null,

    init() {

    },

    playlist(playlist, owner_id, hash) {
        const self = this;
        const MIDDAY_PLAYLIST = 'vk_playlist_-2000688606_3688606';
        self.function = function () {
            window.VK.Widgets.Playlist(MIDDAY_PLAYLIST, playlist, owner_id, hash);
        };
        return self;
    },

    group() {
        const self = this;
        const MIDDAY_GROUP = 186263402;
        self.function = function () {
            window.VK.Widgets.Group("vk_groups", {mode: 3}, MIDDAY_GROUP);
        };
        return self;
    },
    event(vk_event) {
        const self = this;
        const MIDDAY_GROUP = vk_event;
        //const MIDDAY_GROUP = 190692344;
        self.function = function () {
            window.VK.Widgets.Group("vk_event", {mode: 3}, MIDDAY_GROUP);
        };
        return self;
    },

    run() {
        const self = this;
        self.function();
    }
};

export default VkService;
